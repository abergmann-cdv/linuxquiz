﻿using UnityEngine;
using TMPro;

public class Question : MonoBehaviour
{
    private const int numberOfQuestions = 4;
    [SerializeField] private QuestionData questionData = null;
    [SerializeField, Range(6, 16)] private int questionFontSize = 10;
    [SerializeField, Range(4, 12)] private int answersFontSize = 6;
    [Header("The following fields should not be modified:")]
    [SerializeField] private TextMeshProUGUI questionText = null;
    [SerializeField] private GameObject[] answers = new GameObject[numberOfQuestions];

    private void Awake()
    {
        SetQuestion();
    }

    private void OnValidate() // runs only in editor
    {
        SetQuestion();
    }

    private void SetQuestion()
    {
        questionText.text = questionData.question;
        questionText.fontSize = questionFontSize;
        TextMeshProUGUI tmp;
        for (int i = 0; i < answers.Length; i++)
        {
            tmp = answers[i].GetComponent<TextMeshProUGUI>();
            tmp.text = questionData.answers[i];
            tmp.fontSize = answersFontSize;
            if(questionData.correctAnswer == i + 1)
            {
                answers[i].GetComponent<Answer>().isCorrectAnswer = true;
            }
        }
    }
}
