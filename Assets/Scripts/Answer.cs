﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class Answer : MonoBehaviour
{
    public bool isCorrectAnswer = false; // used in Question.cs
    private bool pointWasAlreadyGranted = false;
    private readonly Color correctAnswerColor = new Color(0f, 1f, 0f);
    private readonly Color incorrectAnswerColor = new Color(1f, 0f, 0f);
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = gameObject.GetComponent<TextMeshProUGUI>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Movement>())
        {
            text.color = isCorrectAnswer ? correctAnswerColor : incorrectAnswerColor;
            if(isCorrectAnswer && !pointWasAlreadyGranted)
            {
                Points.Count += 1;
                pointWasAlreadyGranted = true;
            }
        }
    }
}