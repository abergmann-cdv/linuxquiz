﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private LayerMask _platformsLayerMask;
    [SerializeField] private float speed = 15f;
    private Rigidbody2D _rigidbody2d;
    private BoxCollider2D _boxCollider2D;
    float jumpVelocity;

    private void Awake()
    {
        _rigidbody2d = transform.GetComponent<Rigidbody2D>();
        _boxCollider2D = transform.GetComponent<BoxCollider2D>();
    }

    private void Update()
    {
        transform.Translate(Vector2.right * Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime);

        if (_isGrounded() && Input.GetKeyDown(KeyCode.W))
        {
            jumpVelocity = 50f;
            _rigidbody2d.velocity = Vector2.up * jumpVelocity;
        }
    }

    private bool _isGrounded()
    {
        RaycastHit2D _raycastHit2D = Physics2D.BoxCast(_boxCollider2D.bounds.center, _boxCollider2D.bounds.size, 0f, Vector2.down, .1f, _platformsLayerMask);
        return _raycastHit2D.collider != null;
    }
}