﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class PointsDisplay : MonoBehaviour
{
    private TextMeshProUGUI text;
    private int lastPointCount;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        if(lastPointCount != Points.Count)
        {
            text.text = "POINTS: " + Points.Count;
            lastPointCount = Points.Count;
        }
    }
}